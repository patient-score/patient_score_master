﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using AppMODEL;
using Npgsql;
using OfficeOpenXml;


namespace APPDB.repository
{
    public class Repository
    {

        string constr = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;

        public DataTable ConvertXSLXtoDataTable(string strFilePath, ExcelPackage package, string filename)
        {
            ExcelWorksheet worksheet = package.Workbook.Worksheets.First();
            DataTable dt = new DataTable();
            var start_row = 2;


            foreach (var firstrowcell in worksheet.Cells[start_row, 1, start_row, worksheet.Dimension.End.Column])
            {
                dt.Columns.Add(firstrowcell.Text);
            }
            var noOfRecords = 0;
            for (var rownum = start_row + 1; rownum <= worksheet.Dimension.End.Row; rownum++)
            {
                var row = worksheet.Cells[rownum, 1, rownum, worksheet.Dimension.End.Column];
                bool RowIsEmpty = true;

                var newRow = dt.NewRow();
                foreach (var cell in row)
                {
                    if (cell.Value != null)
                    {
                        RowIsEmpty = false;
                    }
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                if (RowIsEmpty == false)
                {
                    dt.Rows.Add(newRow);
                    noOfRecords += 1;
                }


            }
            var res = addCurseReport(dt, strFilePath, filename);
            // HttpContext.Current.Session["error"] = noOfRecords;
            return dt;




        }

        public String ConvertXSLXtoDataTableAPI(string strFilePath, ExcelPackage package, string filename)
        {
            ExcelWorksheet worksheet = package.Workbook.Worksheets.First();

            DataTable dt = new DataTable();
            var start_row = 2;
            foreach (var firstrowcell in worksheet.Cells[start_row, 1, start_row, worksheet.Dimension.End.Column])
            {
                dt.Columns.Add(firstrowcell.Text);
            }
            for (var rownum = start_row + 1; rownum <= worksheet.Dimension.End.Row; rownum++)
            {

                var row = worksheet.Cells[rownum, 1, rownum, worksheet.Dimension.End.Column];
                bool RowIsEmpty = true;

                var newRow = dt.NewRow();
                foreach (var cell in row)
                {
                    if (cell.Value != null)
                    {
                        RowIsEmpty = false;
                    }
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                if (RowIsEmpty == false)
                {
                    dt.Rows.Add(newRow);
                }
            }
            var res = addCurseReport(dt, strFilePath, filename);

            return res;




        }

        public string addCurseReport(DataTable dt, string filepath, string filename)
        {
            try
            {


                var limit = dt.Rows.Count;
                using (NpgsqlConnection con = new NpgsqlConnection(constr))
                {
                    con.Open();
                    string s = @"""PATIENT""";
                    string s2 = @"""ALL_DRUGS""";
                    string s3 = @"""tblErrorLog""";
                    string s4 = @"""DRUGADMIN""";
                    string s5 = @"""color_mapping""";
                    string lastname = dt.Rows[0]["last name"].ToString();
                    string firstname = dt.Rows[0]["first name"].ToString();
                    string DOB = dt.Rows[0]["DOB"].ToString();
                    string Gender = dt.Rows[0]["Gender"].ToString();
                    string address = dt.Rows[0]["Address"].ToString();
                    string Drugname = dt.Rows[0]["drug name"].ToString();

                    int patientid = 0;
                    using (NpgsqlCommand cmd = new NpgsqlCommand("insert into " + s.ToString() + "(last_name,first_name,dob,gender,updated_ts,address) values('" + lastname + "','" + firstname + "','" + DOB + "','" + Gender + "','" + DateTime.Now.ToString() + "','" + address + "') RETURNING Pid", con))

                    {
                        int yy = (Int32)cmd.ExecuteScalar();

                        patientid = yy;


                    }
                    for (int i = 0; i < limit; i++)
                    {


                        string datefilled = dt.Rows[i]["Date Filled"].ToString();
                        string datesold = dt.Rows[i]["Date Sold"].ToString();
                        string compact = dt.Rows[i]["Compact"].ToString();
                        string drugname = dt.Rows[i]["Drug Name"].ToString();

                        string form = dt.Rows[i]["form"].ToString();
                        string DrugStrength = dt.Rows[i]["Drug Strength"].ToString();
                        string Qty = dt.Rows[i]["Qty"].ToString();
                        string DaysSupply = dt.Rows[i]["Days Supply"].ToString();

                        string SpeciesCode = dt.Rows[i]["Species Code"].ToString();
                        string Rx = dt.Rows[i]["Rx#"].ToString();
                        string Refill = dt.Rows[i]["Refill#"].ToString();
                        string RefillsAuthorized = dt.Rows[i]["Refills Authorized"].ToString();

                        string PaymentMethod = dt.Rows[i]["Payment Method"].ToString();
                        string PrescriberName = dt.Rows[i]["Prescriber Name"].ToString();
                        string PrescDEA = dt.Rows[i]["Presc. DEA#"].ToString();

                        string PharmacyName = dt.Rows[i]["Pharmacy Name"].ToString();
                        string Pharmacy = dt.Rows[i]["Pharmacy#"].ToString();
                        drugname = drugname.Trim();
                        //var dmsg = "- drugname(" + drugname + ")";

                        // HttpContext.Current.Session["error"] = dmsg;

                        //  return dmsg;



                        string colorvalue = "";

                        using (NpgsqlCommand cmd2 = new NpgsqlCommand("select * from " + s2.ToString() + " WHERE propname ILIKE '%" + drugname + "%' or curse_drugname ILIKE '%" + drugname + "%' ", con))
                        {
                            NpgsqlDataReader dr3 = cmd2.ExecuteReader();

                            if (dr3.HasRows)
                            {
                                dr3.Read();
                                string drugid = dr3["drug_id"].ToString();
                                string color_id = dr3["color_id"].ToString();
                                try
                                {

                                    using (NpgsqlConnection con2 = new NpgsqlConnection(constr))
                                    {
                                        con2.Open();
                                        using (NpgsqlCommand cmd4 = new NpgsqlCommand("select * from " + s5.ToString() + " WHERE color_id ='" + color_id + "' ", con2))
                                        {
                                            NpgsqlDataReader drx = cmd4.ExecuteReader();
                                            while (drx.Read())
                                            {
                                                colorvalue = drx["color_value"].ToString();
                                            }
                                            drx.Close();
                                        }
                                        con2.Close();
                                    }
                                }
                                catch (Exception ex)
                                {

                                    var errorMsg = "Errorr in reading color value - color id(" + color_id + "), Error - " + ex.Message;
                                    HttpContext.Current.Session["error"] = errorMsg;

                                    return errorMsg;
                                }


                                using (NpgsqlConnection con3 = new NpgsqlConnection(constr))
                                {
                                    con3.Open();
                                    string potency = dr3["potency"].ToString();

                                    using (NpgsqlCommand cmd4 = new NpgsqlCommand("insert into " + s4.ToString() + "(patient_id,date_filled,date_sold,compact,drug_name,form,drug_strength,quantity,days_supply,species_code,rx_number,refill_number,refills_authorized,payment_method,prescriber_name,prescription_dea_number,pharmacy_name,pharmacy_number,updated_ts,drug_id,color_value,potency) values('" + patientid + "','" + datefilled + "','" + datesold + "','" + compact + "','" + drugname + "','" + form + "','" + DrugStrength + "','" + Qty + "','" + DaysSupply + "','" + SpeciesCode + "','" + Rx + "','" + Refill + "','" + RefillsAuthorized + "','" + PaymentMethod + "','" + PrescriberName + "','" + PrescDEA + "','" + PharmacyName + "','" + Pharmacy + "','" + DateTime.Now.ToString() + "','" + drugid + "','" + colorvalue + "','" + potency + "')", con3))
                                    {

                                        dr3.Close();

                                        int x3 = cmd4.ExecuteNonQuery();
                                    }
                                    con3.Close();
                                }

                            }
                            else
                            {
                                dr3.Close();
                                using (NpgsqlCommand cmd4 = new NpgsqlCommand("insert into " + s3.ToString() + " values('" + patientid + "','" + dt.Rows[i]["drug name"] + "','" + DateTime.Now.ToString() + "')", con))
                                {

                                    int x3 = cmd4.ExecuteNonQuery();
                                    // UpdateCurseDrugName(drugname);

                                }
                            }
                        }


                    }
                    try
                    {
                        var copyToPath = HttpContext.Current.Server.MapPath("~/App_Data/Processed/") + filename;

                        // Copy the file
                        File.Move(filepath, copyToPath);

                    }
                    catch (Exception ex)
                    {

                    }
                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }
                    String pid = "";
                    try
                    {
                        pid = patientid.ToString();
                        var ms_result = makeScore(pid);
                        HttpContext.Current.Session["error"] = ms_result;
                        return ms_result;
                    }
                    catch (Exception ex)
                    {
                        // Get stack trace for the exception with source file information
                        var st = new StackTrace(ex, true);
                        // Get the top stack frame
                        var frame = st.GetFrame(0);
                        // Get the line number from the stack frame
                        var line = frame.GetFileLineNumber();

                        var errorMsg = "Errorr in parsing cures report (pid-" + pid + "), Error - " + ex.Message + ", at - " + line;
                        HttpContext.Current.Session["error"] = errorMsg;

                        return errorMsg;
                    }

                }







            }
            catch (Exception ex)
            {
                // Get stack trace for the exception with source file information
                var st = new StackTrace(ex, true);
                // Get the top stack frame
                var frame = st.GetFrame(0);
                // Get the line number from the stack frame
                var line = frame.GetFileLineNumber();
                var errorMsg = "Errorr in parsing cures report , Error - " + ex.Message + ", at - " + ex.StackTrace;
                HttpContext.Current.Session["error"] = errorMsg;

                return errorMsg;


            }
        }

        //public string UpdateCurseDrugName(string drugname)
        //{
        //    string s2 = @"""ALL_DRUGS""";
        //    using (NpgsqlConnection con = new NpgsqlConnection(constr))
        //    {
        //        con.Open();
        //        using (NpgsqlCommand cmd2 = new NpgsqlCommand("select * from " + s2.ToString() + " WHERE propname like '" + drugname + "%' or propname like '%" + drugname + "%'", con))
        //        {
        //            NpgsqlDataReader dr3 = cmd2.ExecuteReader();
        //            if (dr3.HasRows)
        //            {
        //                dr3.Read();
        //                string drugid = dr3["drug_id"].ToString();
        //                using (NpgsqlCommand cmd3 = new NpgsqlCommand("update " + s2.ToString() + " set curse_drugname=" + drugname + " WHERE drug_id=" + drugid + " ", con))
        //                {
        //                    dr3.Close();

        //                    int xx = cmd3.ExecuteNonQuery();

        //                }
        //            }
        //            else
        //            {
        //                return "completed";
        //            }

        //            return "completed";
        //        }
        //    }
        //}


        public string GetPDF(string infile, string outfile)
        {
            ProcessStartInfo start = new ProcessStartInfo();

            start.FileName = @"D:\PatientScoreLive\patient_score_master\Backend\PS_PDFConverter\dist\htmltopdf.exe";
            string args = infile + " " + outfile;

            start.Arguments = args;
            start.UseShellExecute = false;// Do not use OS shell
            start.CreateNoWindow = true; // We don't need new window
            start.RedirectStandardOutput = true; // Any output, gesnerated by application will be redirected back
            start.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
            using (Process process = Process.Start(start))
            {
                process.WaitForExit();
                using (StreamReader reader = process.StandardOutput)
                {
                    int exitCode = process.ExitCode;
                    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                   
                    if (exitCode == 0)
                    {
                        return "SUCCEED";
                    }
                    return stderr;
                }
            }            
                        return "FAILED";
        }
        public string downloadCuresReport(PatientDetailModel ObjUser, string cures_userid, string cures_password)
        {
            string email = HttpContext.Current.Session["loggedin_doc_email"].ToString();

            string s4 = @"""provider""";
            string resp_str = "";
            //byte[] encoded = Convert.FromBase64String(Objuser.password);
            //string ep = System.Text.Encoding.UTF32.GetString(encoded);
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
                resp_str = resp_str + " First level,";
                using (NpgsqlCommand cmd2 = new NpgsqlCommand("select * from " + s4.ToString() + " WHERE email_address = '" + email + "' and is_verified=true", con))
                {
                    NpgsqlDataReader dr3 = cmd2.ExecuteReader();

                    var i = 1;

                    resp_str = resp_str + " Second level,";

                    if (dr3.HasRows)
                    {
                        resp_str = resp_str + " Third level,";
                        while (dr3.Read())
                        {
                            // Since, we are not storing the cures userid nd password in DB
                            //string cures_userid = dr3["cures_userid"].ToString();
                            //string cures_password = dr3["cures_password"].ToString();
                            string patient_firstname = ObjUser.firstname;
                            string patient_lastname = ObjUser.lastname;
                            string patient_dob = ObjUser.dob;



                            ProcessStartInfo start = new ProcessStartInfo();

                            start.FileName = @"D:\PatientScoreLive\patient_score_master\Backend\PS_CuresDownloader\dist\getpar.exe";
                            string args = cures_userid + " " + cures_password + " " + patient_firstname + " " + patient_lastname + " " + patient_dob;

                            //start.FileName = "D:\\PatientScoreLive\\patient_score_master\\cures_downloader\\python\\python.exe";
                            start.Arguments = args;
                            start.UseShellExecute = false;// Do not use OS shell
                            start.CreateNoWindow = true; // We don't need new window
                            start.RedirectStandardOutput = true; // Any output, gesnerated by application will be redirected back
                            start.RedirectStandardError = true; // Any error in standard output will be redirected back (for example exceptions)
                            using (Process process = Process.Start(start))
                            {
                                process.WaitForExit();
                                using (StreamReader reader = process.StandardOutput)
                                {
                                    int exitCode = process.ExitCode;
                                    string stderr = process.StandardError.ReadToEnd(); // Here are the exceptions from our Python script
                                    string result = reader.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                                    if (exitCode == -1)
                                    {
                                        return "Exit code: " + exitCode.ToString() + ", Error: " + stderr;

                                    }
                                    else if (exitCode == 0)
                                    {
                                        return "MATCH";
                                    }
                                    else if (exitCode == 41)
                                    {
                                        return "Cures password is incorrect";
                                    }
                                    else if (exitCode == 44)
                                    {
                                        return "No reports found for the search, Firstname: " + patient_firstname + ", LastName: " + patient_lastname + ", DOB: " + patient_dob;
                                    }
                                    else if (exitCode == 48)
                                    {
                                        return "Unable to process the report!!";
                                    }
                                    else if (exitCode == 43)
                                    {
                                        return "Arguments missing. Please fill firstname/lasname/dob.";
                                    }

                                }
                            }


                            break;
                        }

                    }


                }
            }
            return "NOMATCH";
        }
        public bool loginDoc(Userdetailmodel Objuser)
        {

            string s4 = @"""provider""";
            //byte[] encoded = Convert.FromBase64String(Objuser.password);
            //string ep = System.Text.Encoding.UTF32.GetString(encoded);
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();

                using (NpgsqlCommand cmd2 = new NpgsqlCommand("select * from " + s4.ToString() + " WHERE email_address = '" + Objuser.email_address + "' and  password = '" + Objuser.password + "' and is_verified=true", con))
                {
                    NpgsqlDataReader dr3 = cmd2.ExecuteReader();

                    var i = 1;


                    if (dr3.HasRows)
                    {

                        while (dr3.Read())
                        {
                            HttpContext.Current.Session["loggedin_doc_firstname"] = dr3["first_name"].ToString();
                            HttpContext.Current.Session["loggedin_doc_lastname"] = dr3["last_name"].ToString();
                            HttpContext.Current.Session["loggedin_doc_email"] = dr3["email_address"].ToString();

                            //HttpContext.Current.Session["cures_userid"] = dr3["cures_userid"].ToString();
                            //HttpContext.Current.Session["cures_password"] = dr3["cures_password"].ToString();
                            break;
                        }
                        return true;

                    }
                    else
                    {
                        return false;
                    }

                }
            }



        }



        public string EnableAccount()
        {
            string email = HttpContext.Current.Session["pid"].ToString();

            string s4 = @"""provider""";

            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
                NpgsqlCommand chk = new NpgsqlCommand("select * from " + s4.ToString() + " where email_address ='" + email + "'", con);
                NpgsqlDataReader dd = chk.ExecuteReader();
                if (dd.HasRows)
                {
                    dd.Close();

                    using (NpgsqlCommand cmd2 = new NpgsqlCommand("update " + s4.ToString() + " set is_verified=" + true + " where email_address ='" + email + "'", con))
                    {

                        int xx = cmd2.ExecuteNonQuery();

                        if (xx > 0)
                        {

                            return "Registration Successfull";

                        }
                        else
                        {
                            return "Registration Failed";
                        }

                    }

                }
                else
                {
                    return "No account registered with given email !!!";
                }

            }

        }
        //register doctor
        public string RegisterDoc(Userdetailmodel Objuser)
        {

            string s4 = @"""provider""";

            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
                NpgsqlCommand chk = new NpgsqlCommand("select * from " + s4.ToString() + " where email_address ='" + Objuser.email_address + "'", con);
                NpgsqlDataReader dd = chk.ExecuteReader();



                if (dd.HasRows)
                {
                    return "Email id is already exists!";
                }
                dd.Close();

                NpgsqlCommand chk1 = new NpgsqlCommand("select * from " + s4.ToString() + " where cures_userid ='" + Objuser.cures_userid + "'", con);
                NpgsqlDataReader dd1 = chk1.ExecuteReader();


                if (dd1.HasRows)
                {
                    return "Cures userid is already exists!";
                }
                else
                {

                    dd1.Close();

                    using (NpgsqlCommand cmd2 = new NpgsqlCommand("insert into " + s4.ToString() + " (first_name,last_name,email_address,password,phone_number,liceanse_number,cures_userid,cures_password,update_ts) values('" + Objuser.fname + "','" + Objuser.lastname + "','" + Objuser.email_address + "','" + Objuser.password + "','" + Objuser.Phone + "','" + Objuser.liceanse + "','" + Objuser.cures_userid + "','" + Objuser.cures_password + "','" + DateTime.Now.ToString() + "')", con))
                    {

                        int xx = cmd2.ExecuteNonQuery();

                        if (xx > 0)
                        {

                            return "Registration Successfull";
                        }
                        else
                        {
                            return "Registration Failed";
                        }

                    }
                }
            }

            return "At the end";

        }

        public string LostPassword(Userdetailmodel Objuser)
        {
            string s4 = @"""provider""";
            Random rd = new Random();
            int x = rd.Next(12345, 90000);
            string mainp = "ps" + x.ToString();
            //byte[] encoded = Convert.FromBase64String(mainp);
            //string ep = System.Text.Encoding.UTF32.GetString(encoded);

            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
                NpgsqlCommand chk = new NpgsqlCommand("select * from " + s4.ToString() + " where email_address ='" + Objuser.email_address + "'", con);

                NpgsqlDataReader dd = chk.ExecuteReader();
                if (dd.HasRows)
                {
                    return "1";


                }
                else
                {
                    return "2";
                }
            }

        }
        public bool ChangeCuresCredentials(Userdetailmodel Objuser)
        {
            string email = HttpContext.Current.Session["loggedin_doc_email"].ToString();


            string s4 = @"""provider""";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();

                NpgsqlCommand pc = new NpgsqlCommand("update " + s4.ToString() + " set cures_password='" + Objuser.cures_password + "' where email_address='" + email + "'", con);

                int xx = pc.ExecuteNonQuery();
                if (xx > 0)
                {
                    HttpContext.Current.Session["cures_userid"] = Objuser.cures_userid;
                    HttpContext.Current.Session["cures_password"] = Objuser.cures_password;

                    return true;
                }
                return false;
            }
        }
        public bool ChangePassword(Userdetailmodel Objuser)
        {
            string email = HttpContext.Current.Session["pid"].ToString();


            string s4 = @"""provider""";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();

                NpgsqlCommand pc = new NpgsqlCommand("update " + s4.ToString() + " set password=" + Objuser.password + " where email_address='" + email + "'", con);

                int xx = pc.ExecuteNonQuery();
                if (xx > 0)
                {
                    return true;
                }
                return false;
            }


        }

        public String makeScore(string pid)
        {

            string s4 = @"""DRUGADMIN""";
            string t5 = @"""patient_score_ai""";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
                //return "select * from " + s4.ToString() + " where patient_id=" + pid + "";
                //  Updated on 29/09/2019 , to get the prescriptions in ascending order 

                NpgsqlCommand pc = new NpgsqlCommand("select * from " + s4.ToString() + " where patient_id=" + pid + " order by date_sold asc", con);
                float nstrength = 0;
                float totalScore = 0;
                float medscore = 0;
                float medstrength = 0;
                float potency = 0;
                string dbug_msg = "";
                NpgsqlDataReader dr = pc.ExecuteReader();


                while (dr.Read())
                {

                    if (string.IsNullOrEmpty(dr["potency"].ToString()) == false)
                    {
                        if (dr["potency"].ToString() == "9999")
                        {
                            //potency = 0;
                            continue;

                        }
                        else
                        {
                            //try
                            //{
                            potency = float.Parse(dr["potency"].ToString());
                            //}
                            //catch(Exception ex)
                            //{
                            //     potency = 1;
                            //}   
                        }

                        //        dbug_msg += "te: " + dr["potency"].ToString() + ";";
                        //int dose = int.Parse(dr["potency"].ToString());
                        string payment = dr["payment_method"].ToString();
                        string strength = dr["drug_strength"].ToString();
                        string qty = dr["quantity"].ToString();
                        string days = dr["days_supply"].ToString();


                        if (strength == null)
                        {
                            strength = "1MG";
                        }
                        if (payment == "Cash" || payment == "Card")
                        {

                            if (strength.Contains('-') || strength.Contains('/'))
                            {


                                string ss = "";
                                try
                                {
                                    string[] arr = strength.Split(new Char[] { '-', '/' });
                                    ss = arr[1];
                                    ss = Regex.Replace(ss, "[^0-9.]+", string.Empty); //To remove non  integer character

                                    char[] charsToTrim = { ' ', '\t' };
                                    ss = ss.Trim(charsToTrim);

                                    nstrength = (int)Convert.ToDouble(ss.ToString());
                                    float tdosage = nstrength * int.Parse(days);
                                    float calscore = (potency * tdosage) - 3;
                                    totalScore = totalScore + calscore;

                                    // dbug_msg = "Days - " + days + ", strengh -" + ss + ", potency-" + potency.ToString() + ", tdosage - " + tdosage.ToString() + "\n";
                                }
                                catch (Exception ex)
                                {
                                    return ex.Message + " - " + strength + " strength (" + ss + ")";

                                }

                            }



                            else
                            {


                                string ss = "";
                                try
                                {

                                    ss = Regex.Replace(strength, "[^0-9.]+", string.Empty); //To remove non  integer character

                                    char[] charsToTrim = { ' ', '\t' };
                                    ss = ss.Trim(charsToTrim);

                                    nstrength = (int)Convert.ToDouble(ss.ToString());
                                    float tdosage = nstrength * int.Parse(days);
                                    float calscore = (potency * tdosage) + 2;
                                    totalScore = totalScore + calscore;
                                    // dbug_msg = "Days - " + days + ", strengh -" + ss + ", potency-" + potency.ToString() + ", tdosage - " + tdosage.ToString() + "\n";

                                }
                                catch (Exception ex)
                                {
                                    return ex.Message + " - " + strength + " strength (" + ss + ")";

                                }




                            }
                        }
                        else
                        {
                            if (strength.Contains('-') || strength.Contains('/'))
                            {


                                string ss = "";
                                try
                                {

                                    string[] arr = strength.Split(new Char[] { '-', '/' });
                                    ss = arr[1];
                                    ss = Regex.Replace(ss, "[^0-9.]+", string.Empty); //To remove non  integer character

                                    char[] charsToTrim = { ' ', '\t' };
                                    ss = ss.Trim(charsToTrim);


                                    nstrength = (int)Convert.ToDouble(ss.ToString());


                                    float calscore = (potency * nstrength) + 2;
                                    totalScore = totalScore + calscore;
                                    // dbug_msg = "Days - " + days + ", strengh -" + ss + ", potency-" + potency.ToString() +  "\n";

                                }
                                catch (Exception ex)
                                {
                                    return ex.Message + " - " + strength + " strength (" + ss + ")";

                                }

                            }



                            else
                            {


                                string ss = "";
                                try
                                {

                                    ss = Regex.Replace(strength, "[^0-9.]+", string.Empty); //To remove non  integer character

                                    char[] charsToTrim = { ' ', '\t' };
                                    ss = ss.Trim(charsToTrim);


                                    nstrength = (int)Convert.ToDouble(ss.ToString());

                                    float calscore = (potency * nstrength) + 2;
                                    totalScore = totalScore + calscore;
                                    //dbug_msg = "Days - " + days + ", strengh -" + ss + ", potency-" + potency.ToString() + "\n";

                                }
                                catch (Exception ex)
                                {
                                    return ex.Message + " - " + strength + " strength (" + ss + ")";

                                }

                            }
                        }


                        // Add all the potency drugs for medd score calculation, except empty potency drugs
                        if (potency != 9999) // 9999 means Empty potency 
                        {

                            //medd score calculation
                            if (strength.Contains('-') || strength.Contains('/'))
                            {



                                string ss = "";
                                try
                                {
                                    string[] arr = strength.Split(new Char[] { '-', '/' });

                                    ss = arr[1];
                                    ss = Regex.Replace(ss, "[^0-9.]+", string.Empty); //To remove non  integer character
                                    char[] charsToTrim = { ' ', '\t' };
                                    ss = ss.Trim(charsToTrim);


                                    nstrength = (int)Convert.ToDouble(ss.ToString());

                                    medstrength = (int)Convert.ToDouble(ss.ToString());

                                    float calmedscore = (potency * nstrength * int.Parse(qty)) / int.Parse(days);
                                    //medscore = medscore + calmedscore; // Do not sum the MEDD score of each prescription

                                    //FIXME, need to insert MEDD score of each presecription 
                                    medscore = calmedscore;

                                    //dbug_msg += "Days - " + days + ", strengh -" + nstrength.ToString() + ", potency-" + potency.ToString()+" calmedscore: " +calmedscore.ToString()+", medd: "+medscore.ToString()+ "\n";
                                }
                                catch (Exception ex)
                                {
                                    return ex.Message + " - " + strength + " strength (" + ss + ")";

                                }
                            }
                            else
                            {
                                string ss = "";
                                try
                                {
                                    ss = Regex.Replace(strength, "[^0-9.]+", string.Empty); //To remove non  integer character
                                    char[] charsToTrim = { ' ', '\t' };
                                    ss = ss.Trim(charsToTrim);

                                    nstrength = (int)Convert.ToDouble(ss.ToString());

                                    float calmedscore = (potency * nstrength * int.Parse(qty)) / int.Parse(days);

                                    //medscore = medscore + calmedscore; // Do not sum the MEDD score of each prescription

                                    //FIXME, need to insert MEDD score of each presecription 

                                    medscore = calmedscore;

                                    //dbug_msg += "Days - " + days + ", strengh -" + nstrength.ToString() + ", potency-" + potency.ToString() + " calmedscore: " + calmedscore.ToString() + ", medd: " + medscore.ToString() + "\n";


                                }
                                catch (Exception ex)
                                {
                                    return ex.Message + " - " + strength + " strength (" + ss + ")";
                                }



                                //        dr.Close();
                            }
                        }



                    }

                }

                using (NpgsqlCommand updatescore = new NpgsqlCommand("insert into " + t5.ToString() + " values('" + pid + "','" + pid + "','" + totalScore + "','" + medscore + "','" + DateTime.Now + "') ", con))
                {
                    dr.Close();
                    int xx = updatescore.ExecuteNonQuery();
                    if (xx > 0)
                    {
                        return "Successfully score generated for patient - " + pid;
                    }
                    else
                    {
                        return "Failed to generate score for patient ( - " + pid;
                    }
                }
                //return dbug_msg;

            }

            return "Failed to generate for patient - " + pid;




        }

        public List<Score> getOrders(string pid)
        {
            string T1 = @"""DRUGADMIN""";
            string T2 = @"""PATIENT""";
            string prepot = "0";
            string pot = "";
            List<Score> scorelist = new List<Score>();
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select  a.*,b.first_name,b.last_name  from " + T1 + " a join " + T2 + " b on a.patient_id=b.pid where b.pid='" + pid + "'", con);
                cmd.CommandType = CommandType.Text;
                con.Open();

                NpgsqlDataReader rdr = cmd.ExecuteReader();
                var i = 1;
                while (rdr.Read())
                {
                    /*
                     * 
                     date_filled, date_sold, compact, drug_name, form, drug_strength, quantity, days_supply, species_code, 
                     rx_number, refill_number, refills_authorized, payment_method, prescriber_name, prescription_dea_number, 
                     pharmacy_name, pharmacy_number, updated_ts, drug_id, color_value,potency

                     */
                    Score order = new Score();
                    order.PatientName = rdr["first_name"].ToString() + rdr["last_name"].ToString();
                    order.drugname = rdr["drug_name"].ToString();
                    order.potency = rdr["potency"].ToString();
                    order.drugStrength = rdr["drug_strength"].ToString();
                    order.colorvalue = rdr["color_value"].ToString();

                    order.date_filled = rdr["date_filled"].ToString();
                    order.date_sold = rdr["date_sold"].ToString();
                    order.compact = rdr["compact"].ToString();

                    order.form = rdr["form"].ToString();
                    order.quantity = rdr["quantity"].ToString();
                    order.days_supply = rdr["days_supply"].ToString();
                    order.species_code = rdr["species_code"].ToString();
                    order.rx_number = rdr["rx_number"].ToString();
                    order.refill_number = rdr["refill_number"].ToString();
                    order.refills_authorized = rdr["refills_authorized"].ToString();
                    order.payment_method = rdr["payment_method"].ToString();
                    order.prescriber_name = rdr["prescriber_name"].ToString();
                    order.prescription_dea_number = rdr["prescription_dea_number"].ToString();
                    order.pharmacy_name = rdr["pharmacy_name"].ToString();
                    order.pharmacy_number = rdr["pharmacy_number"].ToString();
                    order.drug_id = rdr["drug_id"].ToString();


                    if (string.IsNullOrEmpty(rdr["potency"].ToString()) == true)
                    {
                        prepot = "0";

                    }
                    else
                    {
                        prepot = rdr["potency"].ToString();
                    }
                    float potencyvalue = float.Parse(prepot);

                    if (potencyvalue == 1)
                    {
                        pot = "m";

                    }
                    else if (potencyvalue > 1 && potencyvalue != 9999)
                    {
                        pot = "h";

                    }
                    else if (potencyvalue < 1)
                    {
                        pot = "l";

                    }
                    else if (potencyvalue == 9999)
                    {
                        pot = "e";

                    }

                    order.filterElement = pot;
                    scorelist.Add(order);

                    i++;
                }

            }
            return scorelist;

        }



        public List<Score> Chart(string pid)
        {
            string T1 = @"""DRUGADMIN""";
            string T2 = @"""PATIENT""";
            string prepot = "";
            string pot = "";
            List<Score> scorelist = new List<Score>();
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select  a.drug_name,a.potency,a.drug_strength,b.first_name,b.last_name,b.dob,b.address,a.color_value  from " + T1 + " a join " + T2 + " b on a.patient_id=b.pid where b.pid='" + pid + "'", con);
                cmd.CommandType = CommandType.Text;
                con.Open();

                NpgsqlDataReader rdr = cmd.ExecuteReader();
                var i = 1;
                while (rdr.Read())
                {
                    Score order = new Score();
                    order.PatientName = rdr["first_name"].ToString() + " " + rdr["last_name"].ToString();
                    order.PatientDob = rdr["dob"].ToString();
                    order.PatientAddress = rdr["address"].ToString();

                    order.drugname = rdr["drug_name"].ToString();
                    order.potency = rdr["potency"].ToString();
                    order.drugStrength = rdr["drug_strength"].ToString();
                    order.colorvalue = rdr["color_value"].ToString();



                    scorelist.Add(order);

                    i++;
                }

            }
            return scorelist;

        }





        //positive potency
        public string hightPotency(string pid)
        {
            string T1 = @"""DRUGADMIN""";
            string T2 = @"""PATIENT""";
            var i = 0;
            string color = "";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select *  from " + T1 + "   where patient_id='" + pid + "' and potency > '1' and potency != '9999'", con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                Score order = new Score();
                NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {

                    color = rdr["color_value"].ToString();

                    i++;
                }

            }
            return i + "-" + color;

        }

        //negative
        public string lowPotency(string pid)
        {
            string T1 = @"""DRUGADMIN""";
            string T2 = @"""PATIENT""";
            var i = 0;
            string color = "";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select *  from " + T1 + "   where patient_id='" + pid + "' and potency < '1' and potency !=''", con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                Score order = new Score();
                NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {

                    color = rdr["color_value"].ToString();

                    i++;
                }

            }
            return i + "-" + color;

        }


        public string mediumPotency(string pid)
        {
            string T1 = @"""DRUGADMIN""";
            string T2 = @"""PATIENT""";
            var i = 0;
            string color = "";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select *  from " + T1 + "   where patient_id='" + pid + "' and  potency = '1' ", con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                Score order = new Score();
                NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {

                    color = rdr["color_value"].ToString();

                    i++;
                }

            }
            return i + "-" + color;

        }
        public string EmptyPotency(string pid)
        {
            string T1 = @"""DRUGADMIN""";
            string T2 = @"""PATIENT""";
            var i = 0;
            string color = "";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select *  from " + T1 + "   where patient_id='" + pid + "' and  potency = '9999' ", con);
                cmd.CommandType = CommandType.Text;
                con.Open();
                Score order = new Score();
                NpgsqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {

                    color = rdr["color_value"].ToString();

                    i++;
                }

            }
            return i + "-" + color;

        }
        public List<PatientDetailModel> PSearchBylfd(String lname, String fname, String dob = "")
        {
            var user = new List<PatientDetailModel>();
            string T2 = @"""PATIENT""";


            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select * from " + T2 + " where dob ilike '%" + dob + "%' and first_name ilike '%" + fname + "%' and last_name ilike '%" + lname + "%'", con);

                cmd.CommandType = CommandType.Text;
                con.Open();

                NpgsqlDataReader rdr = cmd.ExecuteReader();
                var i = 1;

                while (rdr.Read())
                {
                    PatientDetailModel order = new PatientDetailModel();
                    order.patient_id = rdr["pid"].ToString();
                    order.firstname = rdr["first_name"].ToString();
                    order.lastname = rdr["last_name"].ToString();
                    order.email = rdr["email_id"].ToString();
                    order.Adres = rdr["address"].ToString();
                    order.dob = rdr["dob"].ToString();




                    user.Add(order);



                }

            }

            return user.ToList();
        }
        public List<PatientDetailModel> PSearch(String search_key, String search_val)
        {
            var user = new List<PatientDetailModel>();
            string T2 = @"""PATIENT""";


            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select * from " + T2 + " where CAST(" + search_key + " AS TEXT) ilike '%" + search_val + "%'", con);

                cmd.CommandType = CommandType.Text;
                con.Open();

                NpgsqlDataReader rdr = cmd.ExecuteReader();
                var i = 1;

                while (rdr.Read())
                {
                    PatientDetailModel order = new PatientDetailModel();
                    order.patient_id = rdr["pid"].ToString();
                    order.firstname = rdr["first_name"].ToString();
                    order.lastname = rdr["last_name"].ToString();
                    order.email = rdr["email_id"].ToString();
                    order.Adres = rdr["address"].ToString();
                    order.dob = rdr["dob"].ToString();



                    user.Add(order);



                }

            }

            return user.ToList();
        }

        public List<PatientDetailModel> GetPatient(PatientDetailModel ObjUser)
        {
            string T2 = @"""PATIENT""";

            var user = new List<PatientDetailModel>();
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                string dob = ObjUser.dob;
                if (dob != null) {
                    dob = Regex.Replace(dob, "/", "-"); //To replace / with -

                }

                NpgsqlCommand cmd = new NpgsqlCommand("select * from " + T2 + " where dob ilike '%" + dob + "%' and first_name ilike '%" + ObjUser.firstname + "%' and last_name ilike '%" + ObjUser.lastname + "%'", con);


                cmd.CommandType = CommandType.Text;
                con.Open();

                NpgsqlDataReader rdr = cmd.ExecuteReader();
                var i = 1;

                while (rdr.Read())
                {
                    PatientDetailModel order = new PatientDetailModel();
                    order.patient_id = rdr["pid"].ToString();
                    order.firstname = rdr["first_name"].ToString();
                    order.lastname = rdr["last_name"].ToString();
                    order.dob = rdr["dob"].ToString();

                    order.email = rdr["email_id"].ToString();
                    order.Adres = rdr["address"].ToString();



                    user.Add(order);



                }

            }

            return user.ToList();
        }

        public string Postfile(HttpPostedFileBase Csv)
        {

            HttpContext.Current.Server.ScriptTimeout = 30000;

            var fileName = Path.GetFileName(Csv.FileName);

            var vpath = Path.Combine(
                HttpContext.Current.Server.MapPath("~/App_Data/Processed"),
                fileName
            );

            Csv.SaveAs(vpath);
            var c = UploadCsv(vpath);

            return c;



        }




        //loginc for  adding data into database
        public string UploadCsv(string vpath)
        {

            HttpContext.Current.Server.ScriptTimeout = 50000;


            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();


                string s = @"""ALL_DRUGS""";

                NpgsqlCommand cmd = new NpgsqlCommand("COPY PUBLIC." + s.ToString() + " FROM  '" + vpath + "' WITH CSV HEADER  encoding 'windows-1251'", con);


                int xx = cmd.ExecuteNonQuery();


                NpgsqlCommand cmd2 = new NpgsqlCommand("alter table " + s.ToString() + " add column drug_id serial", con);
                int yy = cmd2.ExecuteNonQuery();

                NpgsqlCommand cmd3 = new NpgsqlCommand("alter table " + s.ToString() + " add column color_id integer", con);

                int colorid = cmd3.ExecuteNonQuery();



                NpgsqlCommand cmd4 = new NpgsqlCommand("update " + s.ToString() + " set color_id='4' where potency='1'", con);
                int zeropot = cmd4.ExecuteNonQuery();
                NpgsqlCommand cmd5 = new NpgsqlCommand("update " + s.ToString() + " set color_id='3' where potency < '1'", con);
                int negpot = cmd5.ExecuteNonQuery();


                NpgsqlCommand cmd6 = new NpgsqlCommand("update " + s.ToString() + " set color_id='2' where potency > '1'", con);
                int popot = cmd6.ExecuteNonQuery();

                NpgsqlCommand cmd8 = new NpgsqlCommand("update " + s.ToString() + " set potency='9999' where potency is null", con);
                int fill_empty_potency = cmd8.ExecuteNonQuery();

                NpgsqlCommand cmd7 = new NpgsqlCommand("update " + s.ToString() + " set color_id='1' where potency='9999' ", con);
                int emptypot = cmd7.ExecuteNonQuery();








            }


            return "Inserted SuccessFully";
        }
        public string DeletePar()
        {
            string s1 = @"""PATIENT""";
            string s2 = @"""patient_score_ai""";
            string s3 = @"""tblErrorLog""";
            string s4 = @"""DRUGADMIN""";
            List<string> listofpids = new List<string>();
            string query="";
            try
            {
                using (NpgsqlConnection con = new NpgsqlConnection(constr))
                {
                    string pid ;
                    con.Open();
                    //SELECT * FROM public."tblErrorLog" WHERE TO_TIMESTAMP(updated_Ts, 'MM/DD/YYYY HH24:MI:SS') < NOW() - INTERVAL '5 minutes';
                    // Psql server time is 1 hour ahead, so i put 1 hour 5 minutes as interval
                    using (NpgsqlCommand cmd2 = new NpgsqlCommand("select * from " + s1.ToString() + " WHERE TO_TIMESTAMP(updated_Ts, 'MM/DD/YYYY HH24:MI:SS') < NOW() - INTERVAL '1 hour 5 minutes'", con))
                    {
                        NpgsqlDataReader dr3 = cmd2.ExecuteReader();

                        if (dr3.HasRows)
                        {
                            while (dr3.Read())
                            {
                                pid = dr3["pid"].ToString();
                                listofpids.Add(pid);

                            }
                            dr3.Close();

                        }
                    }
                    for (int i = 0; i < listofpids.Count; i++) // Loop through List with for
                    {
                        pid=listofpids[i];
                       // query= "delete from table " + s1.ToString() + " where pid = '" + pid + "'";
                    //Patient score AI
                     NpgsqlCommand cmd = new NpgsqlCommand("delete from " + s2.ToString() + " where address_id='" + pid + "'", con);
                     cmd.ExecuteNonQuery();
                    // Patient data
                        NpgsqlCommand cmd4 = new NpgsqlCommand("delete from " + s1.ToString() + " where pid = '" + pid + "'", con);
                    cmd4.ExecuteNonQuery();
                    // Tbl error log
                    //NpgsqlCommand cmd5 = new NpgsqlCommand("delete from " + s3.ToString() + " where Patient_id= '" + pid + "'", con);
                    //cmd5.ExecuteNonQuery();
                    // Drug admin
                    NpgsqlCommand cmd6 = new NpgsqlCommand("delete from " + s4.ToString() + " where patient_id= '" + pid + "'", con);
                    cmd6.ExecuteNonQuery();
                }

            }

                return "DELETED";
            }
            catch (Exception ex)
            {
                return "FAILED: "+ ex.Message +" E: "+query;
            }

        }
        public bool DeleteAllTable()
        {
            string s1 = @"""PATIENT""";
            string s2 = @"""patient_score_ai""";
            string s3 = @"""tblErrorLog""";
            string s4 = @"""DRUGADMIN""";
            try
            {
                using (NpgsqlConnection con = new NpgsqlConnection(constr))
                {

                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand("truncate table " + s2 + " ", con);
                    cmd.ExecuteNonQuery();

                    NpgsqlCommand cmd4 = new NpgsqlCommand("truncate table " + s1 + " ", con);
                    cmd4.ExecuteNonQuery();


                    NpgsqlCommand cmd5 = new NpgsqlCommand("truncate table " + s3 + " ", con);
                    cmd5.ExecuteNonQuery();


                    NpgsqlCommand cmd6 = new NpgsqlCommand("truncate table " + s4 + " ", con);
                    cmd6.ExecuteNonQuery();
                }

                return true;
            }
            catch (Exception ex)
            {
                return true;
            }

        }

        public bool DeleteTable()
        {
            string s = @"""ALL_DRUGS""";
            try
            {
                using (NpgsqlConnection con = new NpgsqlConnection(constr))
                {

                    con.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand("truncate table " + s + " RESTART IDENTITY", con);
                    cmd.ExecuteNonQuery();



                    NpgsqlCommand cmd2 = new NpgsqlCommand("alter  table " + s + " drop column color_id ", con);
                    cmd2.ExecuteNonQuery();

                    NpgsqlCommand cmd3 = new NpgsqlCommand("alter table " + s + " drop column drug_id", con);
                    cmd3.ExecuteNonQuery();

                }

                return true;
            }
            catch (Exception ex)
            {
                return true;
            }

        }


        public string ScoreSum(string pid)
        {
            string T2 = @"""patient_score_ai""";
            string Field = @"""ID""";
            string score = "";
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                NpgsqlCommand cmd = new NpgsqlCommand("select * from " + T2 + " where  " + Field + "=" + pid + "  ", con);
                cmd.CommandType = CommandType.Text;
                con.Open();

                NpgsqlDataReader rdr = cmd.ExecuteReader();

                if (rdr.Read())
                {
                    score = rdr["patient-score"].ToString() + ":" + rdr["MEDD"].ToString();
                }

            }

            return score;

        }
    }
}