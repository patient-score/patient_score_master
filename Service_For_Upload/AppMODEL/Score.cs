﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMODEL
{
   public class Score
    {
        public string PatientName { get; set; }
        public string PatientDob { get; set; }
        public string PatientAddress { get; set; }

        public string drugname { get; set; }
        public string potency { get; set; }

        public string drugStrength { get; set; }

        public string filterElement { get; set; }

        public string colorvalue { get; set; }

        public string date_filled { get; set; }
        public string date_sold { get; set; }
        public string compact { get; set; }
        public string drug_name { get; set; }
        public string form { get; set; }
        public string drug_strength { get; set; }
        public string quantity { get; set; }
        public string days_supply { get; set; }
        public string species_code { get; set; }
        public string rx_number { get; set; }
        public string refill_number { get; set; }
        public string refills_authorized { get; set; }
        public string payment_method { get; set; }
        public string prescriber_name { get; set; }
        public string prescription_dea_number { get; set; }
        public string pharmacy_name { get; set; }
        public string pharmacy_number { get; set; }
        public string drug_id { get; set; }
       


}
}
