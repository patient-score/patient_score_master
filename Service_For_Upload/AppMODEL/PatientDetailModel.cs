﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMODEL
{
   public class PatientDetailModel
    {
        public string patient_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string dob { get; set; }
        public string  Adres { get; set; }

    }
}
