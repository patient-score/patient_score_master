﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMODEL
{
   public class Userdetailmodel
    {
        public string fname { get; set; }
        public string lastname { get; set; }
        public string email_address { get; set; }
        public string Phone { get; set; }
        public string liceanse { get; set; }
        public string password { get; set; }
        public string dob { get; set; }
        public string patient_id { get; set; }
        public string cures_userid { get; set; }
        public string cures_password { get; set; }
    }
}
