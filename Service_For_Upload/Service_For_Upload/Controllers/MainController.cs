﻿using APPDB.repository;
using AppMODEL;
using ExcelDataReader;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Text;
using System.Net;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System;
using System.IO;
namespace Service_For_Upload.Controllers
{
    public class MainController : Controller
    {
        Repository repos = null;
       
        public MainController()
        {
            repos = new Repository();
                
                
                }


        public ActionResult Index()
        {
            return View("Index");
        }

        public String Say()
        {
            return "Hey";
        }
       
        [HttpPost]
        public ActionResult PostMedicine()
        {
            return View("Index");

        }

        public ActionResult AddPateintRecord()
        {
            return View();
        }

        [HttpPost]
        public String PatientRecordAPI(HttpPostedFileBase patientFile)
        {
            var fileName = System.IO.Path.GetFileName(patientFile.FileName);

            var vpath = System.IO.Path.Combine(
                Server.MapPath("~/App_Data/ToBeProcessed"),
                fileName
            );


            patientFile.SaveAs(vpath);

            ExcelPackage package = new ExcelPackage(patientFile.InputStream);
            var res = repos.ConvertXSLXtoDataTableAPI(vpath, package, patientFile.FileName);

            return res;
        }

        [HttpPost]
        public ActionResult PatientRecord(HttpPostedFileBase patientFile)
        {
            var fileName = System.IO.Path.GetFileName(patientFile.FileName);

            var vpath = System.IO.Path.Combine(
                Server.MapPath("~/App_Data/ToBeProcessed"),
                fileName
            );


            patientFile.SaveAs(vpath);

            ExcelPackage package = new ExcelPackage(patientFile.InputStream);
            DataTable dt = repos.ConvertXSLXtoDataTable(vpath, package, patientFile.FileName);
            ViewBag.error = Session["error"].ToString();
            return View("AddPateintRecord");
        }


        public ActionResult UploadCsv(HttpPostedFileBase csv)
        {
            return View();
        }


        public ActionResult Landing()
        {
            return View();

        }

        public ActionResult LostPassword()
        {
            return View();
        }
        private DateTime _LastAccessedTime;
        public Boolean CheckSessionTimeout()
        {
            /*
if (Session["cures_userid"] != null)
{
    ViewBag.cures_userid = Session["cures_userid"].ToString();
    ViewBag.cures_password = Session["cures_password"].ToString();

}
*/
            if (Request.Cookies["cures"] != null)
            {
                HttpCookie _cookie = Request.Cookies["cures"];
                ViewBag.cures_userid = _cookie["cures_userid"].ToString();
                ViewBag.cures_password = Base64Decode(_cookie["cures_password"].ToString());
            }

            else
            {
                ViewBag.cures_userid = null;
            }

            if (Session["LastAccessedTime"] != null)
            {
           
                _LastAccessedTime = Convert.ToDateTime(Session["LastAccessedTime"]);

                TimeSpan elapsedtimespan = DateTime.Now.Subtract(_LastAccessedTime);
                int elapsedtime = Convert.ToInt32(elapsedtimespan.TotalMinutes);
                if (elapsedtime >= 10 )
                {
                    //If the elapsed time between last access and new access time  is more than 5 minutes thn session expired
                    return true;
                }


            }
            else
            {
                return true;
            }
            Session["LastAccessedTime"] = DateTime.Now; // To update the last accessed time 
       
           
            return false;
        }
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Userdetailmodel Objuser)
        {
            var res = repos.loginDoc(Objuser);
           
            if(res==true)
            {
                Session["LastAccessedTime"] = DateTime.Now; // To initialize last accessed time

                return RedirectToAction("FindScore");
            }
            else
            {
                ViewBag.msg = "Wrong Username or Password / Account not yet verified..!";
            }
            return View();
        }

        public ActionResult VerifyAccount()
        {
            Session["pid"] = Request.QueryString["id"].ToString();

            if (Session["encpass"].ToString() == Request.QueryString["AccessToken"].ToString())
            {
               var res = repos.EnableAccount();
               return RedirectToAction("Login");
            }

            return RedirectToAction("Register");
        }
        public String Testmail()
        {
            var email_address = "ananthakannan14@gmail.com";

            WebMail.SmtpServer = "smtpout.secureserver.net";

            WebMail.SmtpPort = 587;
            WebMail.SmtpUseDefaultCredentials = true;

            WebMail.EnableSsl = true;

           WebMail.UserName = "support@patient-score.com";
           WebMail.Password = "lalitha12";

            var msg = "Hi,. Greetings from Patient-Score Team, We welcome you to our portal on your successful sign-up with us..To activate your account and to start enjoying our valuable services, please click on the link given below: <a href='http://patient-score.com/Main/VerifyAccount?id=" + email_address + "'>verify me</a>  Contact Us if Not Requested By you <br>Patient-score.com";

            WebMail.From = "support@patient-score.com";

            //Send email  
            WebMail.Send(to: email_address,from: "support@patient-score.com", replyTo: "noreply@gmail.com", subject: "Verify your account", body: msg, isBodyHtml: true);

            return "Mail sent";
        }

        public static bool ReCaptchaPassed(string gRecaptchaResponse)
        {
            HttpClient httpClient = new HttpClient();
            var res = httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret=6Ledf8MUAAAAAB-LG1PSFj5S7PJ1xUCv3Wg8XTea&response={gRecaptchaResponse}").Result;
            if (res.StatusCode != HttpStatusCode.OK)
                return false;

            string JSONres = res.Content.ReadAsStringAsync().Result;
            dynamic JSONdata = JObject.Parse(JSONres);
            if (JSONdata.success != "true")
                return false;

            return true;
        }

        [HttpPost]
        public ActionResult Register(Userdetailmodel Objuser)
        {
            if (!ReCaptchaPassed(Request.Form["retoken"]))
            {
                ViewBag.msg="You failed the CAPTCHA.";
                return View();
            }
            

            var res = repos.RegisterDoc(Objuser);
            
            if(res== "Registration Successfull")
            {
                // For temporary
                //ViewBag.msg = "Successfully registered..";
                //return View();
                Random rd = new Random();
                int x = rd.Next(12345, 56789);
                Session["encpass"] = "VerificationId" + x.ToString();

                try
                {
                    WebMail.SmtpServer = "smtpout.secureserver.net";

                    WebMail.SmtpPort = 587;
                    WebMail.SmtpUseDefaultCredentials = true;

                    WebMail.EnableSsl = true;

                    WebMail.UserName = "support@patient-score.com";
                    WebMail.Password = "lalitha12";

                    

                    var msg = "Hi,. Greetings from Patient-Score Team, We welcome you to our portal on your successful sign-up with us..To activate your account and to start enjoying our valuable services, please click on the link given below: <a href='http://patient-score.com/Main/VerifyAccount?id=" + Objuser.email_address + "&AccessToken=" + Session["encpass"].ToString() + "'>verify me</a>  Contact Us if Not Requested By you <br>Patient-score.com";


                    WebMail.From = "support@patient-score.com";
               

                    //Send email  
                    WebMail.Send(to: Objuser.email_address, replyTo: "noreply@gmail.com", subject:"Verify your account", body: msg, isBodyHtml: true);

                    ViewBag.msg = "Account verification Link Has Been sent to Your Email Address";

                    return View();

                }
                catch (Exception ex)
                {
                    return RedirectToAction("Login");
                }

            }
            ViewBag.msg = res.ToString();
            return View();
        }

        public ActionResult Register()
        {
            ViewBag.msg = "";
            return View();

        }





        [HttpPost]
        public ActionResult LostPassword(Userdetailmodel Objuser)
        {
           var res=repos.LostPassword(Objuser);
            if (res == "0")
            {
                ViewBag.msg = "Failed to Update Password";
            }
            else if (res == "2")
            {
                ViewBag.msg = "Email Not Registered";

            }
            else
            {
                Random rd = new Random();
                int x = rd.Next(12345, 56789);
                Session["encpass"] = "PasswordResetId" + x.ToString();

                try {
            

                    WebMail.SmtpServer = "smtpout.secureserver.net";

                    WebMail.SmtpPort = 587;
                    WebMail.SmtpUseDefaultCredentials = true;

                    WebMail.EnableSsl = true;

                    WebMail.UserName = "support@patient-score.com";
                    WebMail.Password = "lalitha12";

                    var msg = "Hello...! You have Requested For a Password Change click to Reset Pasword <a href='http://patient-score.com/Main/ChangePassword?id="+Objuser.email_address+"&AccessToken=" + Session["encpass"].ToString() + "'> Reset </a>  Contact Us if Not Requested By you <br>Patient-score.com";


                    WebMail.From = "support@patient-score.com";

                    //Send email  
                    WebMail.Send(to: Objuser.email_address, replyTo: "noreply@gmail.com", subject: "Reset Password", body: msg, isBodyHtml: true);

                    ViewBag.msg = "A Password Reset Link Has Been sent to Your Email Address";
                    return View();
                }
                catch(Exception ex)
                {
                    return RedirectToAction("Login");
                }
                }
            return RedirectToAction("Login");

        }

        public ActionResult ChangePassword()
        {

            Session["pid"] = Request.QueryString["id"].ToString();

            if (Session["encpass"].ToString() == Request.QueryString["AccessToken"].ToString())
            {

                if (Session["encpass"].ToString() != null)
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("LostPassword");
                }

            }
            else
            {
                return RedirectToAction("LostPassword");
            }



        }

        [HttpPost]
        public ActionResult ChangePassword(Userdetailmodel Objuser)
        {
           var res=repos.ChangePassword(Objuser);
             if(res==true)
            {
                return RedirectToAction("Login");
            }
            else
            {
                ViewBag.msg = "Failed To Update Password";
                return View("LostPassword");
            }

        }

        [HttpPost]
        public ActionResult UpdateCures(Userdetailmodel Objuser)
        {
            var is_expired = CheckSessionTimeout();

            if (is_expired == true)
            {
                ViewBag.msg = "Session Expired..!";

                return RedirectToAction("Logout");
            }

            var res = repos.ChangeCuresCredentials(Objuser);
            if (res == true)
            {
                ViewBag.msg = "Updated the cures User ID and password";
                return View("FindScore");
            }



            else
            {
                ViewBag.msg = "Failed To Update Cures User ID and Password";
                return View("FindScore");
            }

        }


        public ActionResult CheckScore()
        {
            return View();

        }

     
        public ActionResult CheckScoreAuto(string pid)
        {
            var is_expired = CheckSessionTimeout();

            if (is_expired == true)
            {
                ViewBag.msg = "Session Expired..!";

                return RedirectToAction("Logout");
            }

            if (pid == null)
            {
                var mid = Session["pid"].ToString();
                pid = mid;
            }
            Session["Patientid"] = pid;
            Session["pid"] = pid;
            var res = repos.Chart(pid);
          
          
            if (res != null)
            {
                List<Score> scorelist = new List<Score>();

                string color1 = "";

                StringBuilder dataSource = new StringBuilder();

                string pot = "";
                string str = "";
                string prepot = "0";
                float potencyvalue = 0;
                string ppos = repos.hightPotency(pid);
                string[] xppos = ppos.Split('-');
                int high = int.Parse(xppos[0].ToString());
                string poscolor = xppos[1].ToString();

                string npos = repos.lowPotency(pid);
                string[] xnpos = npos.Split('-');
                int low = int.Parse(xnpos[0].ToString());
                string negcolor = xnpos[1].ToString();

                string zpos = repos.mediumPotency(pid);
                string[] znpos = zpos.Split('-');
                int med = int.Parse(znpos[0].ToString());
                string zerocolor = znpos[1].ToString();

                string epos = repos.EmptyPotency(pid);
                string[] enpos = epos.Split('-');
                int empty = int.Parse(enpos[0].ToString());
                string emptycolor = enpos[1].ToString();


                float pct = 0;
                float pct_full = 0;

                int count = high + low + med + empty;
                string[] iscore= new string[2];
                string pot_expand ="";
                try
                {
                    string scroes = repos.ScoreSum(pid);
                    iscore = scroes.Split(':');
                    ViewBag.PatientScore = iscore[0].ToString();
                    //ViewBag.PatientScore=scroes;
                    ViewBag.Medd = iscore[1].ToString();
                    ViewBag.PatientName = res[0].PatientName;
                    //ViewBag.PatientName = count;

                    ViewBag.PatientDob = res[0].PatientDob;
                    ViewBag.PatientAddress = res[0].PatientAddress;
                } 
               catch (Exception ex)
            {
                ViewBag.PatientScore = "Not Calculated";
                ViewBag.Medd = "Not Calculated";
            }

            if (count == high)
                {
                    pct = 50;
                    pot = "H";
                    color1 = "" + poscolor + "";
                    pot_expand = "High Potency";
                }
                else if (count == low)
                {
                    pct = 50;
                    pot = "L";
                    color1 = "" + negcolor + "";
                    pot_expand = "Low Potency";

                }
                else if (count == med)
                {
                    pot = "M";
                    pct = 50;
                    color1 = "" + zerocolor + "";
                    pot_expand = "Medium Potency";

                }
                else if (count == empty)
                {
                    pct = 50;
                    pot = "E";
                    color1 = "" + emptycolor + "";
                    pot_expand = "Empty Potency";

                }
                else
                {
                    for (int x = 0; x < 4; x++)
                    {
                        if (x == 0)
                        {
                            pot = "H";
                            pct = high;
                            pct = (float)(50 * high) / count;

                            pct_full = (float)(Math.Truncate((double)(100 * high) / count));

                            color1 += poscolor + ",";

                            str += "{'label': '" + pot +": "+pct_full+"%' , 'value':'" + pct + "' ,'tooltext': 'High Potency'},";

                        }
                        else if (x == 1)
                        {
                            pot = "L";
                            pct =low;
                            pct = (float)(50 * low) / count;
                            pct_full = (float)(Math.Truncate((double)(100 * low) / count));

                            color1 += negcolor + ",";
                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'Low Potency'},";

                        }
                        else if (x == 2)
                        {
                            pot = "M";
                            pct = med;
                            pct = (float)(50 *med) / count;

                            pct_full = (float)(Math.Truncate((double)(100 * med) / count));

                            color1 += zerocolor + ",";
                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'Medium Potency'},";

                        }
                        else if (x == 3)
                        {
                            pot = "E";
                            pct = empty;
                            pct = (float)(50 * empty) / count;
                            pct_full = (float)(Math.Truncate((double)(100 * empty) / count));

                            color1 += emptycolor;
                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'Empty Potency'},";

                        }

                        //str += "{'label': '" + pot +": "+pct_full+"%' , 'value':'" + pct + "' ,'tooltext': 'Patient score: " + String.Format("{0:0.00}", float.Parse(iscore[0].ToString())) + "'},";

                    }

                }




                //Response.Write(newCountry.CountryName + "</br>" + newCountry.Percentage + "</br>");

                ViewBag.color =color1;


                if (pct == 50)
                    {

                    //           str = "{'label': '" + pot + ": 100%', 'value':'" + pct + "','tooltext':'Patient Score: "+ iscore[0].ToString() + "' },";
                               str = "{'label': '" + pot + ": 100%', 'value':'" + pct + "','tooltext':'"+ pot_expand + "' },";

                     color1 += ",#ff7c6b";
                    }
                    else
                    {
                    //High,Low,Median,Empty,Medd (Colors order)
                    // #FFC0CB =pink
                    // #ff0000 =Red
                    // #FFA500 =Orange
                    //#135e08=Green

                    //color1 = "#ff0000,#FFA500,#FFC0CB,#135e08,#f542cb";


                   color1 = "#ff0000,#FFA500,#ff8b1f,#12e03b,#fc83f8";

                }


                str += "{'label': 'MEDD: " + String.Format("{0:0.00}", float.Parse(iscore[1].ToString())) + " ' , 'value': '50', 'showValue': '0','tooltext': 'MEDD Score: " + iscore[1].ToString() + " '},";


               






                

               

           //     dataSource.Append("{'chart': {'theme': 'fusion', 'caption': '', 'numbersuffix':'%', 'enableMultiSlicing': '0', 'paletteColors': '" + color1 + "', 'bgColor': '#ffffff', 'showBorder': '0', 'use3DLighting': '1', 'showShadow': '0', 'enableSmartLabels': '1','labelDistance': '1', 'startingAngle': '180', 'showLabels': '1', 'showPercentValues': '0', 'showLegend': '0',  'showValues': '0', 'captionFontSize': '12',  'toolTipColor': '#ffffff', 'toolTipBorderThickness': '0', 'toolTipBgColor': '#000000', 'toolTipBgAlpha': '80', 'toolTipBorderRadius': '2', 'showPercentInTooltip': '0','showPercentValues': '0', 'toolTipPadding': '5', },'data': [");

                dataSource.Append("{'chart': {'theme': 'fusion', 'caption': '', 'pieRadius':'70', 'numbersuffix':'%', 'enableMultiSlicing': '0', 'paletteColors': '" + color1 + "', 'bgColor': '#ffffff', 'showBorder': '0', 'use3DLighting': '1', 'showShadow': '0', 'enableSmartLabels': '1', 'startingAngle': '310', 'showLabels': '1', 'showPercentValues': '0', 'showLegend': '0',  'showValues': '0', 'captionFontSize': '14',  'toolTipColor': '#ffffff', 'toolTipBorderThickness': '0', 'toolTipBgColor': '#000000', 'toolTipBgAlpha': '80', 'toolTipBorderRadius': '2', 'showPercentInTooltip': '0','showPercentValues': '0', 'toolTipPadding': '5', },'data': [");

                dataSource.Append(str);
               dataSource.Append("] }");


                //FusionCharts.Charts.Chart newChart = new FusionCharts.Charts.Chart("doughnut3d", "myChart", "100%", "100%", "json", dataSource.ToString());
                FusionCharts.Charts.Chart newChart = new FusionCharts.Charts.Chart("doughnut3d", "myChart", "450", "200", "json", dataSource.ToString());

                newChart.AddEvent("dataplotclick", "clickHandler");
                Session["chart"] = newChart.Render();
            }

         
          return View("DougnatView");


        }

      
        public JsonResult DougnatView()
        {
           
            try
            {
                string pid = Session["pid"].ToString();

                var res = repos.getOrders(pid);
                
                return Json(new { data= res}, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult drugview()
        {
            string pid = Session["Patientid"].ToString();

            var res = repos.getOrders(pid);

            return Json(res, JsonRequestBehavior.AllowGet);
        }


        public ActionResult generatescore()
        {
            return View();
        }

        [HttpPost]
        public ActionResult generatescore(string pid="0")
        {
            if(pid=="0")
            {
                pid = Session["pid"].ToString();
            }

            var res = repos.makeScore(pid);
           
                ViewBag.msg = res;

          
            return View("generatescore");
        }


        public ActionResult FindScore()
        {
            // For debugging purpose
            //_LastAccessedTime = Convert.ToDateTime(Session["LastAccessedTime"]);

            //TimeSpan elapsedtimespan = DateTime.Now - _LastAccessedTime;
            //ViewBag.msg = _LastAccessedTime + " Session time str -"+ Session["LastAccessedTime"] + " Now -"+ DateTime.Now + " elapsed time " + elapsedtimespan.TotalMinutes.ToString();
            var is_expired = CheckSessionTimeout();


            if (is_expired == true)
            {
                ViewBag.msg = "Session Expired..!";

                 return RedirectToAction("Logout");
            }

            if (string.IsNullOrEmpty(Session["loggedin_doc_firstname"] as string))
            {
                return RedirectToAction("Login");

            }
            return View();
        }
        public ActionResult Certificates() {
            var is_expired = CheckSessionTimeout();

            if (is_expired == true)
            {
                ViewBag.msg = "Session Expired..!";

                return RedirectToAction("Logout");
            }

            return View();
        }
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(ControllerContext,
                                                                         viewName,null);

                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
        public string saveas()
        {

           var pid = Session["pid"].ToString();
            Session["Patientid"] = pid;
            Session["pid"] = pid;
            var res = repos.Chart(pid);


            if (res != null)
            {
                List<Score> scorelist = new List<Score>();

                string color1 = "";

                StringBuilder dataSource = new StringBuilder();

                string pot = "";
                string str = "";
                string prepot = "0";
                float potencyvalue = 0;
                string ppos = repos.hightPotency(pid);
                string[] xppos = ppos.Split('-');
                int high = int.Parse(xppos[0].ToString());
                string poscolor = xppos[1].ToString();

                string npos = repos.lowPotency(pid);
                string[] xnpos = npos.Split('-');
                int low = int.Parse(xnpos[0].ToString());
                string negcolor = xnpos[1].ToString();

                string zpos = repos.mediumPotency(pid);
                string[] znpos = zpos.Split('-');
                int med = int.Parse(znpos[0].ToString());
                string zerocolor = znpos[1].ToString();

                string epos = repos.EmptyPotency(pid);
                string[] enpos = epos.Split('-');
                int empty = int.Parse(enpos[0].ToString());
                string emptycolor = enpos[1].ToString();


                float pct = 0;
                float pct_full = 0;

                int count = high + low + med + empty;
                string[] iscore = new string[2];
                string pot_expand = "";
                try
                {
                    string scroes = repos.ScoreSum(pid);
                    iscore = scroes.Split(':');
                    ViewBag.PatientScore = iscore[0].ToString();
                    //ViewBag.PatientScore=scroes;
                    ViewBag.Medd = iscore[1].ToString();
                    ViewBag.PatientName = res[0].PatientName;
                    //ViewBag.PatientName = count;

                    ViewBag.PatientDob = res[0].PatientDob;
                    ViewBag.PatientAddress = res[0].PatientAddress;
                }
                catch (Exception ex)
                {
                    ViewBag.PatientScore = "Not Calculated";
                    ViewBag.Medd = "Not Calculated";
                }

                if (count == high)
                {
                    pct = 50;
                    pot = "H";
                    color1 = "" + poscolor + "";
                    pot_expand = "High Potency";
                }
                else if (count == low)
                {
                    pct = 50;
                    pot = "L";
                    color1 = "" + negcolor + "";
                    pot_expand = "Low Potency";

                }
                else if (count == med)
                {
                    pot = "M";
                    pct = 50;
                    color1 = "" + zerocolor + "";
                    pot_expand = "Medium Potency";

                }
                else if (count == empty)
                {
                    pct = 50;
                    pot = "E";
                    color1 = "" + emptycolor + "";
                    pot_expand = "Empty Potency";

                }
                else
                {
                    for (int x = 0; x < 4; x++)
                    {
                        if (x == 0)
                        {
                            pot = "H";
                            pct = high;
                            pct = (float)(50 * high) / count;

                            pct_full = (float)(Math.Truncate((double)(100 * high) / count));

                            color1 += poscolor + ",";

                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'High Potency'},";

                        }
                        else if (x == 1)
                        {
                            pot = "L";
                            pct = low;
                            pct = (float)(50 * low) / count;
                            pct_full = (float)(Math.Truncate((double)(100 * low) / count));

                            color1 += negcolor + ",";
                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'Low Potency'},";

                        }
                        else if (x == 2)
                        {
                            pot = "M";
                            pct = med;
                            pct = (float)(50 * med) / count;

                            pct_full = (float)(Math.Truncate((double)(100 * med) / count));

                            color1 += zerocolor + ",";
                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'Medium Potency'},";

                        }
                        else if (x == 3)
                        {
                            pot = "E";
                            pct = empty;
                            pct = (float)(50 * empty) / count;
                            pct_full = (float)(Math.Truncate((double)(100 * empty) / count));

                            color1 += emptycolor;
                            str += "{'label': '" + pot + ": " + pct_full + "%' , 'value':'" + pct + "' ,'tooltext': 'Empty Potency'},";

                        }

                        //str += "{'label': '" + pot +": "+pct_full+"%' , 'value':'" + pct + "' ,'tooltext': 'Patient score: " + String.Format("{0:0.00}", float.Parse(iscore[0].ToString())) + "'},";

                    }

                }




                //Response.Write(newCountry.CountryName + "</br>" + newCountry.Percentage + "</br>");

                ViewBag.color = color1;


                if (pct == 50)
                {

                    //           str = "{'label': '" + pot + ": 100%', 'value':'" + pct + "','tooltext':'Patient Score: "+ iscore[0].ToString() + "' },";
                    str = "{'label': '" + pot + ": 100%', 'value':'" + pct + "','tooltext':'" + pot_expand + "' },";

                    color1 += ",#ff7c6b";
                }
                else
                {
                    //High,Low,Median,Empty,Medd (Colors order)
                    // #FFC0CB =pink
                    // #ff0000 =Red
                    // #FFA500 =Orange
                    //#135e08=Green

                    //color1 = "#ff0000,#FFA500,#FFC0CB,#135e08,#f542cb";


                    color1 = "#ff0000,#FFA500,#ff8b1f,#12e03b,#fc83f8";

                }


                str += "{'label': 'MEDD: " + String.Format("{0:0.00}", float.Parse(iscore[1].ToString())) + " ' , 'value': '50', 'showValue': '0','tooltext': 'MEDD Score: " + iscore[1].ToString() + " '},";













                //     dataSource.Append("{'chart': {'theme': 'fusion', 'caption': '', 'numbersuffix':'%', 'enableMultiSlicing': '0', 'paletteColors': '" + color1 + "', 'bgColor': '#ffffff', 'showBorder': '0', 'use3DLighting': '1', 'showShadow': '0', 'enableSmartLabels': '1','labelDistance': '1', 'startingAngle': '180', 'showLabels': '1', 'showPercentValues': '0', 'showLegend': '0',  'showValues': '0', 'captionFontSize': '12',  'toolTipColor': '#ffffff', 'toolTipBorderThickness': '0', 'toolTipBgColor': '#000000', 'toolTipBgAlpha': '80', 'toolTipBorderRadius': '2', 'showPercentInTooltip': '0','showPercentValues': '0', 'toolTipPadding': '5', },'data': [");

                dataSource.Append("{'chart': {'theme': 'fusion', 'caption': '', 'pieRadius':'70', 'numbersuffix':'%', 'enableMultiSlicing': '0', 'paletteColors': '" + color1 + "', 'bgColor': '#ffffff', 'showBorder': '0', 'use3DLighting': '1', 'showShadow': '0', 'enableSmartLabels': '1', 'startingAngle': '310', 'showLabels': '1', 'showPercentValues': '0', 'showLegend': '0',  'showValues': '0', 'captionFontSize': '14',  'toolTipColor': '#ffffff', 'toolTipBorderThickness': '0', 'toolTipBgColor': '#000000', 'toolTipBgAlpha': '80', 'toolTipBorderRadius': '2', 'showPercentInTooltip': '0','showPercentValues': '0', 'toolTipPadding': '5', },'data': [");

                dataSource.Append(str);
                dataSource.Append("] }");


                //FusionCharts.Charts.Chart newChart = new FusionCharts.Charts.Chart("doughnut3d", "myChart", "100%", "100%", "json", dataSource.ToString());
                FusionCharts.Charts.Chart newChart = new FusionCharts.Charts.Chart("doughnut3d", "myChart", "450", "200", "json", dataSource.ToString());

                newChart.AddEvent("dataplotclick", "clickHandler");
                Session["chart"] = newChart.Render();
            }

            string html=RenderRazorViewToString("DougnatView", null);

            string fileName = @"D:\PatientScoreLive\patient_score_master\Backend\Data\temp\" + pid.ToString()+".html";
            string outpdf = @"D:\PatientScoreLive\patient_score_master\Backend\Data\pdfs\" + pid.ToString() + ".pdf";
            string resp = "";

            try
            {
                // Check if file already exists. If yes, delete it.     
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                // Create a new file     
                System.IO.File.WriteAllText(fileName, html);
                resp=repos.GetPDF(fileName, outpdf);
                if(resp == "SUCCEED")
                {
                    string attachment_name = pid.ToString() + ".pdf";
                    Response.Clear();
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment;filename=\""+ attachment_name + "\"");
                    // edit this line to display ion browser and change the file name
                    Response.BinaryWrite(System.IO.File.ReadAllBytes(outpdf));
                    // gets our pdf as a byte array and then sends it to the buffer
                    Response.Flush();
                    Response.End();
                } 
            }
            catch (Exception Ex)
            {
                return "Something went wrong";
            }
            return resp;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        [HttpPost]
        public ActionResult CuresCredentials(Userdetailmodel ObjUser)
        {
            /*
            if (Session["cures_userid"] != null)
            {
                ViewBag.cures_userid = Session["cures_userid"].ToString();
                ViewBag.cures_password = Session["cures_password"].ToString();
                
            }
            else
            {
                Session["cures_userid"] = ObjUser.cures_userid;
                Session["cures_password"] = ObjUser.cures_password;

                //Response.Cookies["cookie"].Expires = DateTime.Now.AddMinutes(1); // add expiry time
            }

            */
            //create a cookie by using HttpCookie class and add the name of the cookie that is Democookie  
            HttpCookie cookie = new HttpCookie("cures");
            //assign the textBoxes Value on introduction-of-cookies  
            cookie["cures_userid"] = ObjUser.cures_userid.ToString();
            cookie["cures_password"] = Base64Encode(ObjUser.cures_password.ToString());
            //Write the Cookies on the client machine  
            cookie.Expires = DateTime.Now.AddDays(20);
            Response.SetCookie(cookie);
            return RedirectToAction("FindScore");
        }
        [HttpPost]
        public ActionResult FindScore(PatientDetailModel ObjUser)
        {

            var is_expired = CheckSessionTimeout();

            if (is_expired == true)
            {
                ViewBag.msg = "Session Expired..!";

                return RedirectToAction("Logout");
            }

            

            var res = repos.GetPatient(ObjUser);
            if(res.Count()==1)
            {
                Session["pid"] = res[0].patient_id;
                return RedirectToAction("CheckScoreAuto");
            }
            else if(res.Count()==0)
            {
                string cures_userid = "";
                string cures_password = "";
                if (Request.Cookies["cures"] != null)
                {
                    HttpCookie _cookie = Request.Cookies["cures"];
                    cures_userid = _cookie["cures_userid"].ToString();
                    cures_password = Base64Decode(_cookie["cures_password"].ToString());
                }
                else
                {
                    ViewBag.msg ="Please provide your cures user id and password..";
                    return View("FindScore");
                }

                var downloaded_res = repos.downloadCuresReport(ObjUser,cures_userid,cures_password);
                if (downloaded_res == "MATCH")
                {
                    res = repos.GetPatient(ObjUser);
                    if (res.Count() == 1)
                    {
                        Session["pid"] = res[0].patient_id;
                        return RedirectToAction("CheckScoreAuto");
                    }
                    else
                    {
                        ViewBag.msg = "Report processed successfully, Please search again..";
                        return View("FindScore");

                    }
                }
                else
                {
                    ViewBag.msg = downloaded_res;
                    return View("FindScore");
                }
                

               
            }
            else
            {
                return View("ScoreList",res);
            }
            
        }
        [HttpPost]
        public ActionResult SearchPatientByFname(String fname)
        {
            var res = repos.PSearch("first_name",fname);

            return Json(new  {data = res });

        }
        [HttpPost]
        public ActionResult SearchPatientById(String pid)
        {
            var res = repos.PSearch("pid", pid);

            return Json(new { data = res });

        }
        [HttpPost]
        public ActionResult SearchPatientByLname(String lname,String fname)
        {
            var res = repos.PSearchBylfd(lname,fname);

            return Json(new { data = res });

        }

        [HttpPost]
        public ActionResult SearchPatientByDob(String dob, String lname, String fname)
        {
            var res = repos.PSearchBylfd(lname, fname,dob);

            return Json(new { data = res });

        }

        [HttpPost]
        public ActionResult PostFile(HttpPostedFileBase CSV)
        {
            var res = repos.Postfile(CSV);

            ViewBag.msg = "Uploaded SuccessFully";
            return View("Index");
        }
        public string  DeletePAR()
        {
            // This controller will be called from a python worker
            // It will remove the call records that are older than 5 minutes
            var res="";
            res=repos.DeletePar();
            return res;
        }
        public ActionResult DeletePatientData()
        {

            repos.DeleteAllTable();
            ViewBag.msg = "Tables (Patient,Drug_Admin,Patient_Score_AI) are Empty Now";

            return View("Index");
        }
        public ActionResult Droptable()
        {

            repos.DeleteTable();
            ViewBag.msg="Tables (Patient,All_Drugs,Drug_Admin) are Empty Now";

            return View("Index");
        }

       public ActionResult Logout()
        {
            Session.RemoveAll();
           

            return RedirectToAction("Login");
        }

    }
}