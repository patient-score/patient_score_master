﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Npgsql;
using System.Configuration;
using ExcelDataReader;
using System.Data.OleDb;

namespace Service_For_Upload.Controllers
{
    public class DumpApiController : ApiController
    {

        string constr = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;

        [Route("Api/Dump/DumpCsv")]

        public string Postfile()
        {
            var file = HttpContext.Current.Request.Files.Count > 0 ?
     HttpContext.Current.Request.Files[0] : null;

         
                var fileName = Path.GetFileName(file.FileName);

                var vpath = Path.Combine(
                    HttpContext.Current.Server.MapPath("~/Processed"),
                    fileName
                );

                file.SaveAs(vpath);
            var c= UploadCsv(vpath);

            return c;



        }


       

        //loginc for  adding data into database
        public string UploadCsv(string vpath)
        {
           
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
               

                        string s = @"""ALL_DRUGS""";
                   
                        NpgsqlCommand cmd = new NpgsqlCommand("COPY PUBLIC."+s.ToString()+" FROM  '"+vpath+ "' WITH CSV HEADER  encoding 'windows-1251'", con);


                int xx = cmd.ExecuteNonQuery();


                NpgsqlCommand cmd2 = new NpgsqlCommand("alter table " + s.ToString() + " add column drug_id serial", con);
                int yy = cmd2.ExecuteNonQuery();


                }


            return "Inserted SuccessFully";
        }
         
        
        //logic for processing data
    
         public static DataTable ConvertXSLXtoDataTable(string strFilePath, string connString)
        {
            OleDbConnection oledbConn = new OleDbConnection(connString);
            DataTable dt = new DataTable();
            try
            {

                oledbConn.Open();
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", oledbConn))
                {
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    oleda.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    oleda.Fill(ds);

                    dt = ds.Tables[0];
                }
            }
            catch
            {
            }
            finally
            {

                oledbConn.Close();
            }

            return dt;

        }

        public string addCurseReport(DataTable dt,string filepath)
        {
            var limit = dt.Rows.Count;
            using (NpgsqlConnection con = new NpgsqlConnection(constr))
            {
                con.Open();
                string s = @"""PATIENT""";
                string s2 = @"""ALL_DRUG""";
                string s3 = @"""tblErrorLog""";
                string s4 = @"""DRUGADMIN""";
                string lastname = dt.Rows[0]["lastname"].ToString();
                    string firstname = dt.Rows[0]["firstname"].ToString();
                    string DOB = dt.Rows[0]["DOB"].ToString();
                    string Gender = dt.Rows[0]["Gender"].ToString();
                    string address = dt.Rows[0]["Address"].ToString();
                    string Drugname = dt.Rows[0]["drugname"].ToString();

                int patientid = 0;
                    NpgsqlCommand cmd = new NpgsqlCommand("insert into "+s.ToString()+ "(last_name,first_name,dob,gender,update_ts,address) values('" + lastname + "','" + firstname + "','" + DOB + "','" + Gender + "','" + DateTime.Now.ToString() + "','" + address + "')", con);
                    
                        int xx = cmd.ExecuteNonQuery();
                        if(xx>0)
                {
                    NpgsqlCommand cmdx = new NpgsqlCommand("SELECT currval(pg_get_serial_sequence("+s.ToString()+", 'pid') as id");
                    NpgsqlDataReader dr = cmdx.ExecuteReader();
                    while(dr.Read())
                    {
                        patientid = int.Parse(dr["id"].ToString());
                    }
                }

                     for (int i = 0; i < limit; i++)
                     {
                    
                    string doesname = dt.Rows[i]["dosename"].ToString();
                    if(doesname=="TAB")
                    {
                        doesname = "TABLET";
                    }

                    string datefilled = dt.Rows[i]["DateFilled"].ToString();
                    string datesold = dt.Rows[i]["Date Sold"].ToString();
                    string compact = dt.Rows[i]["Compact"].ToString();
                    string drugname = dt.Rows[i]["Drug Name"].ToString();

                    string form = dt.Rows[i]["form"].ToString();
                    string DrugStrength = dt.Rows[i]["Drug Strength"].ToString();
                    string Qty = dt.Rows[i]["Qty"].ToString();
                    string DaysSupply = dt.Rows[i]["Days Supply"].ToString();

                    string SpeciesCode = dt.Rows[i]["Species Code"].ToString();
                    string Rx = dt.Rows[i]["Rx#"].ToString();
                    string Refill = dt.Rows[i]["Refill#"].ToString();
                    string RefillsAuthorized = dt.Rows[i]["Refills Authorized"].ToString();

                    string PaymentMethod = dt.Rows[i]["Payment Method"].ToString();
                    string PrescriberName = dt.Rows[i]["Prescriber Name"].ToString();
                    string PrescDEA = dt.Rows[i]["Presc.DEA#"].ToString();

                    string PharmacyName = dt.Rows[i]["Pharmacy Name"].ToString();
                    string Pharmacy = dt.Rows[i]["Pharmacy#"].ToString();
                   






                    NpgsqlCommand cmd2 = new NpgsqlCommand("select * from " + s2.ToString() + " WHERE propname like '" + dt.Rows[i]["drugname"] + "%'AND dosename like '%"+doesname+"%'", con);
                    NpgsqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        string drugid = dr["drug_id"].ToString();

                        NpgsqlCommand cmd4 = new NpgsqlCommand("insert into " + s4.ToString() + "(patient_id,date_filled,date_sold,compact,drug_name,form,drug_strength,quantity,days_supply,species_code,rx_number,refill_number,refills_authorized,payment_method,prescriber_name,prescription_dea_number,pharmacy_name,pharmacy_number,updated_ts,drug_id) values('" + patientid + "','" + datefilled + "','" +datesold+ "','"+compact+"','"+drugname+"','"+form+"','"+DrugStrength+"','"+Qty+"','"+DaysSupply+"','"+SpeciesCode+"','"+Rx+"','"+Refill+"','"+RefillsAuthorized+"','"+PaymentMethod+"','"+PrescriberName+"','"+PrescDEA+"','"+PharmacyName+"','"+Pharmacy+"','"+DateTime.Now.ToString()+"','"+drugid+"')", con);

                        int x3 = cmd4.ExecuteNonQuery();



    }
                    else { 
                            NpgsqlCommand cmd4 = new NpgsqlCommand("insert into " + s3.ToString() + "(Patient_id,Drugname,updated_ts) values('" + patientid + "','" + Drugname + "','" + DateTime.Now.ToString() + "')", con);

                           int x3 = cmd4.ExecuteNonQuery();
                    }


                }

                var copyToPath = "";


// Copy the file
                File.Copy(filepath, copyToPath);

                if (System.IO.File.Exists(filepath))
                {
                    System.IO.File.Delete(filepath);
                }

            }
            return "Inserted SuccessFully";




        }






    }
}
        
        
        
        
        
           
        


    










    

