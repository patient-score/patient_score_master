import os
import configparser
#import daemon # Only for linux systems
import logging 
import time
import requests
#from daemon import pidfile # Only for LINUX systems
import sys

from datetime import datetime


config = configparser.RawConfigParser()

APP_ROOT=os.path.dirname(os.path.abspath(__file__))

configFilePath = os.path.join(APP_ROOT, "PS_DR.conf")
config.read(configFilePath)
API_URL = config.get('SERVER', 'Host')
END_POINT = config.get('SERVER', 'Endpoint')
URI=API_URL+END_POINT
LOG_DIR=config.get('LOGS', 'LogPath') 
if not os.path.exists(LOG_DIR):
	print("Log directory is not exists -  %s"% LOG_DIR)
	print("INFO: will be terminated in 10 secs ....")
	time.sleep(10)
	sys.exit(2)

logfile = os.path.join(LOG_DIR,'patient_data_removar.log')
print("INFO: Log file created - %s" % logfile)
logging.basicConfig(filename=logfile, level=logging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')
		
while True:
	response = requests.request("POST", URI)
	if response.status_code == 200:
		logging.info("Patient Data removal: %s"%response.text)		
	else:
		logging.error("Something went wrong - Response %s (%s)" % (response.text,response.status_code))
		print("ERROR: please check log ..")


	time.sleep(5*60) # Sleep for 5 minutes
	