import pdfkit
import sys
import logging
import os 
import configparser
config = configparser.RawConfigParser()

APP_ROOT="D:\PatientScoreLive\patient_score_master\Backend\PS_PDFConverter"


configFilePath = os.path.join(APP_ROOT, "ps_htmltopdf_converter.conf")
config.read(configFilePath)

log_dir = config.get('LOGS', 'LogPath')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
    logging.info("Log Directory created - %s " % log_dir)


logfile = os.path.join(log_dir,'par_htmltopdf_converter.log')

logging.basicConfig(filename=logfile, level=logging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')

if __name__ == "__main__":

    logging.info("Started")
    if len(sys.argv) == 3:
        htmlfilepath=sys.argv[1]
        pdffilepath=sys.argv[2]
        pdfkit.from_file(htmlfilepath,pdffilepath)
        logging.info("Converted %s  into %s"%(htmlfilepath,pdffilepath))
    else:
        logging.info("Required arguments missing")

    logging.info("Ended")
