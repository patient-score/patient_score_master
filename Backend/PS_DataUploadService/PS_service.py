
import os
import configparser
#import daemon # Only for linux systems
import logging 
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import requests
#from daemon import pidfile # Only for LINUX systems
import sys
from os import walk
import fnmatch
import shutil
from datetime import datetime


config = configparser.RawConfigParser()

APP_ROOT=os.path.dirname(os.path.abspath(__file__))

configFilePath = os.path.join(APP_ROOT, "PS_service.conf")
config.read(configFilePath)





class Uploader(object):
    def walklevel(self,some_dir, level=1):
        some_dir = some_dir.rstrip(os.path.sep)
        assert os.path.isdir(some_dir)
        num_sep = some_dir.count(os.path.sep)
        for root, dirs, files in os.walk(some_dir):
            yield root, dirs, files
            num_sep_this = root.count(os.path.sep)
            if num_sep + level <= num_sep_this:
                del dirs[:]

    def run(self):        
        
        DIRECTORY_TO_WATCH = config.get('WATCH', 'InDirectoryPath')
        OUT_DIRECTORY = config.get('WATCH', 'OutDirectoryPath')
		
        API_URL = config.get('SERVER', 'Host')
        END_POINT = config.get('SERVER', 'Endpoint')
        
        URI=API_URL+END_POINT

        logging.info("Uploader server URI - %s" % URI)
			
        while True:
            for dirpath, dirs, files in self.walklevel(DIRECTORY_TO_WATCH,0):
                for filename in fnmatch.filter(files, '*.xlsx'):
                    file_path=os.path.join(dirpath, filename)
                    current_date=datetime.today().strftime('%d%m%Y')
                    dst_directory=os.path.join(OUT_DIRECTORY,current_date)

                    dst_file_path=os.path.join(dst_directory, filename)
                    if not os.path.exists(dst_directory):
                        os.mkdir(dst_directory)
                        logging.info("Directory created - %s " % dst_directory)
                    else:    
                        logging.info("Directory already exists - %s" % dst_directory)
                    
                    print("INFO: Processing cures report - %s." % file_path)
					
					
                    logging.info("Processing cures report - %s." % file_path)
                    fobj=open(file_path,'rb')
                    files = {'patientFile': fobj}
                    response = requests.request("POST", URI,files=files)
                    if response.status_code == 200:
                        fobj.close()
                        logging.info("%s %s." % (response.text,file_path))
                        shutil.move(file_path, dst_file_path)
                        print("INFO: %s %s." % (response.text, file_path))
						
						
                    else:
                        fobj.close()
                        logging.error("Something went wrong - Response %s (%s)" % (response.text,response.status_code))
                        print("ERROR: Unable to process the file (" +file_path+"), please check log ..")

	
            time.sleep(10)


class Common(object):
    
    def init(self):
        print("INFO: Patient-Score Data upload service started")
        LOG_DIR=config.get('LOGS', 'LogPath') 
        if not os.path.exists(LOG_DIR):
            logging.error("Log directory is not exists -  %s"% LOG_DIR)
            print("INFO: will be terminated in 10 secs ....")
            time.sleep(10)
            sys.exit(2)
			
        DIRECTORY_TO_WATCH = config.get('WATCH', 'InDirectoryPath')
        OUT_DIRECTORY = config.get('WATCH', 'OutDirectoryPath')
		
        if not os.path.exists(DIRECTORY_TO_WATCH):
            logging.error("Watch directory is not exists -  %s"% DIRECTORY_TO_WATCH)
            print("INFO: will be terminated in 10 secs ....")

            time.sleep(10)
            sys.exit(2)
		
        if not os.path.exists(OUT_DIRECTORY):
            logging.error("Out directory is not exists -  %s"% OUT_DIRECTORY)
            print("INFO: will be terminated in 10 secs ....")
            time.sleep(10)
            sys.exit(2)
        


        logfile = os.path.join(LOG_DIR,'cures_report_uploader.log')
        print("INFO: Log file created - %s" % logfile)

        logging.basicConfig(filename=logfile, level=logging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')
        logging.info("Service started sucessfully")
        uploader=Uploader()
        uploader.run()

                
if __name__ == "__main__":

    common=Common()
    common.init()
