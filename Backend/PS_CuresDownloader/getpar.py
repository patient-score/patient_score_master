import time
import os
import sys
import logging
import configparser
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options  

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import os
import datetime

import time
import glob
import shutil

import requests

config = configparser.RawConfigParser()

APP_ROOT="D:\PatientScoreLive\patient_score_master\Backend\PS_CuresDownloader"

#os.path.dirname(os.path.abspath(__file__))

configFilePath = os.path.join(APP_ROOT, "par_downloader.conf")
config.read(configFilePath)

log_dir = config.get('LOGS', 'LogPath')
if not os.path.exists(log_dir):
    os.mkdir(log_dir)
    logging.info("Log Directory created - %s " % log_dir)


logfile = os.path.join(log_dir,'par_downloader.log')

options = Options()  
options.add_argument("--headless") 


logging.basicConfig(filename=logfile, level=logging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')

cures_dir = config.get('OUTPUT', 'OutDirectoryPath')

if not os.path.exists(cures_dir):
    os.mkdir(cures_dir)
    logging.info("Cures output directory created - %s " % cures_dir)

API_URL = config.get('SERVER', 'Host')
END_POINT = config.get('SERVER', 'Endpoint')

PS_SERVER_URI=API_URL+END_POINT

logging.info("Uploader server URI - %s" % PS_SERVER_URI)

url = config.get('CURES', 'Url')

selenium_driver_path = config.get('COMMON', 'SeleniumDriverPath')

binary = FirefoxBinary(selenium_driver_path)



logging.info("Started...")

# Configure the download location
# To prevent download dialog

options = Options()

tmp_download_dir= os.path.join(log_dir,datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'))


if not os.path.exists(tmp_download_dir):
    os.mkdir(tmp_download_dir)
    logging.info("Temporary download directory created - %s " % tmp_download_dir)


    
options.binary_location = config.get("COMMON","ChromePath") 

#options.headless = True

options.add_experimental_option("prefs", {
  "download.default_directory": tmp_download_dir,
  "download.prompt_for_download": False,
  "download.directory_upgrade": True,
  "safebrowsing.enabled": True
})

    

def finish(driver,status):
    global tmp_download_dir
    # status
    # 41 - Login failed
    # 44 - Report not found

    # Remove temporary download directory
    os.removedirs(tmp_download_dir)
    #time.sleep(200)
    driver.quit()
    sys.exit(status);

def check_cures_report_and_move(driver):

    interval=30   # 2 minutes
    end_at= time.time() + (int(interval))
    is_file_found=0
    while time.time() < end_at:

        file_path=tmp_download_dir+"/par.xlsx"
        logging.info("Checking cures report - %s "%file_path)
        if glob.glob(file_path):

            # File exists, do something
            logging.info("%s File exists"%file_path)
            is_file_found=1
            break
        else:
            logging.info("No .xlsx file found yet..")

        time.sleep(5) # Delay for 1 seconds


    if is_file_found == 0:
        logging.info("Cures report not found")
        finish(driver,44)
    else:
        # move the cures report from tmp directory to actual cures directory
        dst_file=cures_dir+"/par_"+datetime.datetime.now().strftime("%d%m%Y_%H%M%S")+".xlsx"
        
        shutil.move(tmp_download_dir+"/par.xlsx",dst_file)
        fobj=open(dst_file,'rb')
        files = {'patientFile': fobj}
        response = requests.request("POST", PS_SERVER_URI,files=files)
        if response.status_code == 200:
            fobj.close()
            logging.info("%s %s." % (response.text,dst_file))
            finish(driver,0)
            logging.info("Successfully uploaded the report and it is processed")
        else:
            fobj.close()
            logging.error("Something went wrong - Response %s (%s)" % (response.text,response.status_code))

            finish(driver,48)

def start(docter_cures_userid,docter_cures_password,patient_first_name,patient_last_name,patient_dob):
    

    driver = webdriver.Chrome(executable_path=selenium_driver_path,options=options)
    try:
        #patient_dob='06/27/1945'        
        #patient_dob=datetime.datetime.strptime(patient_dob,'%Y-%m-%d').strftime('%m/%d/%Y')
        # Wait for 600 seconds before throwing an "ElementNotVisibleException" exception.
        wait = WebDriverWait(driver, 600)
        driver.get(url)

        # Login
        last_height = driver.execute_script("return document.body.scrollHeight")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        
        # Get elements
        username = driver.find_element_by_id('username')  
        password = driver.find_element_by_id('password')
        submit_button = driver.find_element_by_xpath("//input[@value='Login'][@type='submit']")
        username.send_keys(docter_cures_userid)
        password.send_keys(docter_cures_password)
        logging.info("Filled the user id and password")
        

        ac = ActionChains(driver)
        ac.move_to_element(submit_button).click().perform()
        logging.info("Clicked the submit button")

        # Click patient activity report menu button
        link=None
        try_c=0
        while link is None:

            if try_c == 3:
                logging.error("Tried maximum unable to find 'Patient Activity Report'")
                finish(driver,41)

            try:
                link = driver.find_element_by_link_text('Patient Activity Report')
            except Exception as e:
                logging.info("Unable to locate 'Patient Activity Report' link..")
                time.sleep(5);    # Sleep for 1 second and continue
                
            try_c=try_c+1
        
        link.click()
        logging.info("Clicked the 'Patient Activity Report' menu button..")
        # Click search tab and fill the FIRSTNAME, LASTNAME and DOB and submit search form

        search_tab=None
        try_c=0
        while search_tab is None:

            if try_c == 3:
                logging.error("Tried maximum unable to find 'Search' tab..")
                finish(driver,2)

            try:
                search_tab = driver.find_element_by_link_text('Search')
            except Exception as e:
                logging.info("Unable to locate 'Search' tab..")
                time.sleep(5);    # Sleep for 1 second and continue
                
            try_c=try_c+1
    
        search_tab.click()
        logging.info("Clicked search tab")
        
        search_button=None
        try_c=0
        while search_button is None:

            if try_c == 3:
                logging.error("Tried maximum unable to find search form elements..")
                finish(driver,2)
            try:

                lastname = driver.find_element_by_id('patientSrchForm:parTabs:lastName')
                firstname = driver.find_element_by_id('patientSrchForm:parTabs:firstName')
                dob = driver.find_element_by_id('patientSrchForm:parTabs:dob_input')                 
                search_button = driver.find_element_by_id('patientSrchForm:parTabs:searchBtn')
            except Exception as e:
                logging.info("Unable to locate 'Search form' elements..")
                time.sleep(5);    # Sleep for 1 second and continue
                
            try_c=try_c+1

        logging.info("Got the required elements from search form")

        logging.info("Lastname: %s, firstname: %s,DOB: %s"%(patient_last_name,patient_first_name,patient_dob))
        lastname.send_keys(patient_last_name)
        firstname.send_keys(patient_first_name)
        dob.send_keys(patient_dob)
        logging.info("Filled the last/first name and DOB of patient..")
        
        ac = ActionChains(driver)
        ac.move_to_element(search_button).click().perform()
        logging.info("Clicked the search button")   

        # Click generate Report button

        ## Select all prescriptions and generate report
        try:
            select_all_checkbox=None
            try_c=0
            while select_all_checkbox is None:

                if try_c == 3:
                    logging.error("Tried maximum unable to find select all prescriptions checkbox")
                    finish(driver,44)
                try:
                    xpath="//input[@name='patientSrchForm:parTabs:patientTbl_checkbox'][@type='checkbox']"
                    #xpath="/html/body/div[1]/div[2]/div/div[2]/form/div[3]/div/div[1]/span/span[2]/span/div/div[1]/div/table/thead/tr/th[1]/div/div[1]/input"
                    xpath='//*[@id="patientSrchForm:parTabs:patientTbl:j_idt185"]/div/div[1]/input'
                    select_all_checkbox = driver.find_element_by_xpath(xpath)

                    #select_all_checkbox.click()
                    ac = ActionChains(driver)
                    ac.move_to_element(select_all_checkbox).click().perform()

                    logging.info("Selected all the prescriptions")
                except Exception as e:
                    logging.info("Unable to locate 'Select all the prescriptions' checkbox..")
                    time.sleep(5);    # Sleep for 1 second and continue
                    
                try_c=try_c+1

            

            generate_report_button=None
            try_c=0
            while generate_report_button is None:

                if try_c == 3:
                    logging.error("Tried maximum unable to find out Generate report button..")
                    finish(driver,2)

                try:
                    generate_report_button = driver.find_element_by_id('patientSrchForm:parTabs:patientTbl:genReportBtn')

                    ac = ActionChains(driver)
                    ac.move_to_element(generate_report_button).click().perform()
                    logging.info("Clicked generate report button..")
                except Exception as e:

                    ac.move_to_element(select_all_checkbox).click().perform()
                    logging.info("Unable to locate 'Generate report' button")
                    time.sleep(5);    # Sleep for 1 second and continue
                    
                try_c=try_c+1



            details_tab=None
            try_c=0
            while details_tab is None:

                if try_c == 3:
                    logging.error("Tried maximum unable to find out Details tab..")
                    finish(driver,2)

                try:

                    # Click Details tab and then click Download button
                    details_tab = driver.find_element_by_link_text('Details')

                    ac = ActionChains(driver)
                    ac.move_to_element(details_tab).click().perform()

                    logging.info("Clicked details tab")
                except Exception as e:
                    logging.info("Unable to locate 'Details' tab ..")
                    time.sleep(5);    # Sleep for 1 second and continue
                    
                try_c=try_c+1

            
            download_par_button=None
            try_c=0
            while download_par_button is None:

                if try_c == 3:
                    logging.error("Tried maximum unable to find out Download PAR button tab..")
                    finish(driver,2)

                try:

                    # Click Details tab and then click Download button
                    download_par_button=driver.find_element_by_id('patientSrchForm:parTabs:downloadPar')

                    ac = ActionChains(driver)
                    ac.move_to_element(download_par_button).click().perform()
                    #download_par_button.click()
                    logging.info("Clicked the download button")
                except Exception as e:
                    logging.info("Unable to locate 'Downlod PAR' button ..")
                    time.sleep(5);    # Sleep for 1 second and continue
                    
                try_c=try_c+1


            
            logging.info("Ended...")

            # Watch cures report file in the download directory
            check_cures_report_and_move(driver)

        except Exception as e:
            logging.error("Exception found %s"%e)    
            logging.info("No reports found")
            finish(driver,44)

    except Exception as e:
        print(e)
        logging.error("Exception found %s"%e)    
        finish(driver,1)
    

if __name__ == "__main__":
    # Arguments
    #   1.docter_cures_userid
    #   2.docter_cures_password

    #   3.patient_first_name
    #   4.patient_last_name
    #   5.patient_dob
    if len(sys.argv) == 6:         
        start(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    else:
        logging.info("Required arguments missing")
        sys.exit(43)
